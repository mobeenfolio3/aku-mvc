﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RecordCreation.Models
{
    public class MyLengthAttribute: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value.ToString().Length == 5)
                return true;
            else
                return false;
        }

        
    }
}