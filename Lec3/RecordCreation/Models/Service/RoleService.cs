﻿using RecordCreation.Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecordCreation.Models.Service
{
    public class RoleService
    {
        public List<Role> GetList()
        {
            using (var context = new EF.Entities())
            {
                return context.Roles.ToList();
            }
        }
    }
}