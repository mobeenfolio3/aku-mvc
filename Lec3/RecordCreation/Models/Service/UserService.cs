﻿using RecordCreation.Models.EF;
using RecordCreation.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecordCreation.Models.Service
{
    public partial class UserService : IDisposable
    {
        //This should be in a seperate service for rating
        public List<RatingVM> GetAllRatings(int id)
        {
            var rating = new List<RatingVM>();
            using (var context = new EF.Entities())
            {
                var allRating = context.UserRatings.Where(m=>m.UserId == id).ToList();
                allRating.ForEach(m => {
                    rating.Add
                        (new RatingVM
                        { RatingId = m.RatingID, RatingDateTime = m.RatingDateTime, RatingNo = m.RatingNo }
                        );
                });
            }
            return rating;
        }
        public List<VMUser> GetAllUsers()
        {
            var users = new List<VMUser>();
            using (var context = new EF.Entities())
            {
                var allUsers = context.Users.ToList();
                allUsers.ForEach(m=> { users.Add(Mapper(m)); });
            }
            return users;
        }


        public void DeleteUser(int id)
        {
            using (var context = new EF.Entities())
            {
                var user = context.Users.First(m=>m.UserId == id);
                context.Users.Remove(user);
                context.SaveChanges();
            }
        }

        public VMUser GetUserById(int id)
        {
            using (var context = new EF.Entities())
            {
                return Mapper(context.Users.First(m => m.UserId == id));
            }
        }

        public VMUser Update(VMUser user)
        {

            using (var context = new EF.Entities())
            {
                var role = context.Roles.First(m => m.RoleId == user.RoleId);
                var oldUser = context.Users.First(m => m.UserId == user.UserId);
                oldUser.UserName = user.UserName;
                oldUser.UserEmail = user.UserEmail;
                oldUser.isActive = user.isActive;
                oldUser.UserPassword = user.UserPassword;
                oldUser.Role = role;
                context.SaveChanges();
            }
            return user;
        }

        public VMUser Create(VMUser user)
        {
            
            using (var context = new EF.Entities())
            {
                var role = context.Roles.First(m => m.RoleId == user.RoleId);
                var currentUser = context.Users.Add(Mapper(user, role));
                context.SaveChanges();
                user.UserId = currentUser.UserId;
            }
            return user;
        }

        public void Dispose()
        {
            
        }

        public bool IsValidEmail(string email)
        {
            using (var context = new EF.Entities())
            {
                if (context.Users.Any(m => m.UserEmail == email))
                    return false;
                else
                    return true;
            }
        }

        public bool IsValidUserName(string userName)
        {
            using (var context = new EF.Entities())
            {
                if (context.Users.Any(m => m.UserName == userName))
                    return false;
                else
                    return true;
            }
        }
        private User Mapper(VMUser user, Role role = null)
        {
            return new User
            {
                UserId = user.UserId,
                UserEmail = user.UserEmail,
                isActive = user.isActive,
                UserName = user.UserName,
                UserPassword = user.UserPassword,
                RoleId = user.RoleId,
                Role = role // This saved the role
            };
        }

        public  VMUser Mapper(User user)
        {
            return new VMUser
            {
                UserId = user.UserId,
                UserEmail = user.UserEmail,
                isActive = user.isActive,
                UserName = user.UserName,
                UserPassword = user.UserPassword,
                RoleName = user.Role?.RoleName,
                RoleId = user.RoleId == null? 0: user.RoleId.Value
            };
        }
    }
}