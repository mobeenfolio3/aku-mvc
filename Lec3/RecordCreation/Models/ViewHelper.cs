﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecordCreation.Models.VM
{
    public static class ViewHelper
    {
        public static MvcHtmlString GetListInLI(this HtmlHelper html, List<double> rating)
        {
            var maintag = new TagBuilder("ul");
            
            foreach (var item in rating)
            {
                var tag = new TagBuilder("li");
                tag.SetInnerText(item.ToString());
                maintag.InnerHtml += tag.ToString();
            }

            return new MvcHtmlString(maintag.ToString());
        }
    }
}