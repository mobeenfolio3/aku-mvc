﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecordCreation.Models.VM
{
    public class RatingVM
    {
        public int RatingId { get; set; }
        public double RatingNo { get; set; }
        public System.DateTime RatingDateTime { get; set; }
    }
}