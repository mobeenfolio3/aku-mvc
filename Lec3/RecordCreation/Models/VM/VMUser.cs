﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RecordCreation.Models.VM
{
    public class VMUser
    {
        public int UserId { get; set; }
        [Display(Name = "Email", Order = 2)]
        [EmailAddress]
        [Required]
        public string UserEmail { get; set; }
        [Display(Name = "User Name", Order = 1)]
        [Required(ErrorMessage ="You must enter user name")]
        //[MyLength(ErrorMessage ="My lenght should be 5")]
        public string UserName { get; set; }
        [Display(Name = "Password", Order = 5)]
        [Required]
        public string UserPassword { get; set; }

        [Display(Name = "Retype Password", Order = 5)]
        [Required]
        [Compare("UserPassword",ErrorMessage ="Password is not matching")]
        public string Password2 { get; set; }

        [Display(Name = "Role", Order = 3)]
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        [Display(Name = "Active", Order = 4)]
        public bool isActive { get; set; }
        //public List<VMUser> usersList { get; set; }
    }
}