﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RecordCreation.Models.VM
{
    public class UserRatingVM
    {
        public VMUser Users { get; set; }
        public List<RatingVM> Ratings { get; set; }
    }
}