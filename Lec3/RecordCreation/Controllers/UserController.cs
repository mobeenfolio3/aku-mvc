﻿using RecordCreation.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RecordCreation.Models.Service;

namespace RecordCreation.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            var userList = new List<VMUser>();
            using (var service = new UserService())
            {
                 userList = service.GetAllUsers();
            }
                return View(userList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            GeneratePrefillData();
            //var user = new VMUser();
           // user.usersList = GetUsers();
            return View();
        }


        public PartialViewResult ListOfUsers()
        {
            var lst = GetUsers();
            return PartialView(lst);
        }

        public List<VMUser> GetUsers()
        {
            var ser = new UserService();
            return ser.GetAllUsers();
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (var service = new UserService())
            {
                service.DeleteUser(id);
            }
            return RedirectToAction("Index");
        }

        private void GeneratePrefillData(int roleId = 0)
        {
            var selectionList = new List<SelectListItem>();
            var service = new RoleService();
            var roles = service.GetList();
            foreach (var item in roles)
            {
                if (roleId == 0 || roleId != item.RoleId)
                    selectionList.Add(new SelectListItem { Value = item.RoleId.ToString(), Text = item.RoleName });
                else if (item.RoleId == roleId)
                    selectionList.Add(new SelectListItem { Value = item.RoleId.ToString(), Text = item.RoleName, Selected = true });
            }   
            ViewBag.Roles = selectionList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMUser user)
        {
            if (ModelState.IsValid)
            {
                using (var service = new UserService())
                {
                    if (service.IsValidEmail(user.UserEmail) && service.IsValidUserName(user.UserName))
                    {
                        user = service.Create(user);
                        ViewBag.Message = "User Created.";
                    }
                    else
                    {
                        ModelState.AddModelError("UserEmail", "User alredy exist");
                    }
                }
            }
            else
                ViewBag.Message = "Error In the model. Please Fix";
            //user.usersList = GetUsers();
            GeneratePrefillData();
            return View(user);
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            using (var service = new UserService())
            {
                var user = service.GetUserById(id);
                GeneratePrefillData(user.RoleId);
                return View(user);
            }
            
        }

        [HttpPost]
        public ActionResult Update(VMUser user)
        {
            if (ModelState.IsValid)
            {
                using (var service = new UserService())
                {
                        user = service.Update(user);
                        ViewBag.Message = "User Updated.";
                    return RedirectToAction("Index");
                }
            }
            else
                ViewBag.Message = "Error In the model. Please Fix";

            GeneratePrefillData(user.RoleId);
            return View(user);
        }


        [HttpGet]
        public ActionResult Details(int id)
        {
            using (var service = new UserService())
            {
                var user = service.GetUserById(id);
                var vmUser = new UserRatingVM();
                vmUser.Users = user;
                vmUser.Ratings = service.GetAllRatings(id);
                return View(vmUser);
            }

        }
    }
}