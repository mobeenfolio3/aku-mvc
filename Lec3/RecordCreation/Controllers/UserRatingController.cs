﻿using RecordCreation.Models.EF;
using RecordCreation.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RecordCreation.Controllers
{
    public class UserRatingController : Controller
    {
        // GET: UserRating
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(int Id)
        {
            ViewBag.UserId = Id;
            return View();
        }

        [HttpPost]
        public ActionResult Create(RatingVM rating,string UserId)
        {
            using (var db = new Entities())
            {
                db.UserRatings.Add(new UserRating { RatingDateTime = rating.RatingDateTime, RatingNo = rating.RatingNo, UserId = int.Parse(UserId)});
                db.SaveChanges();
            }
            return RedirectToAction("Details", "User", new { id = UserId });
        }
    }
}