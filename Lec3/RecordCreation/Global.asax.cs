﻿using AutoMapper;
using RecordCreation.Models.EF;
using RecordCreation.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RecordCreation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //https://github.com/AutoMapper/AutoMapper
            //https://www.codeproject.com/Articles/61629/AutoMapper
            //https://dotnetcademy.net/Learn/2/Pages/2
            //var config = new MapperConfiguration(a => a.CreateMap<VMUser, User>().ForMember(s=>s.Role, map => map.MapFrom(s=> new Role { RoleName = s.RoleName, RoleId = s.RoleId })));
            //var mapper = config.CreateMapper();
            //var vmUser = new VMUser { isActive = false, UserEmail = "abc@de.com", UserName = "abcde", UserPassword = "test123" };
            
            //var result = mapper.Map<VMUser, User>(vmUser);
        }
    }
}
