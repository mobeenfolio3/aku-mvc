﻿using Lec5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lec5.Controllers
{
    public class UserController : Controller
    {
        UserService service;
        public UserController()
        {
            service = new UserService();
        }
        // GET: User
        public ActionResult List()
        {
            SetDropDownValues();
           // var list = service.List();
            return View();
        }

        public void SetDropDownValues()
        {
            var list = new List<SelectListItem>();
            var data = service.GetRoles();
            foreach (var item in data)
            {
                list.Add(new SelectListItem { Text = item.RoleName, Value = item.RoleId.ToString() });
            }
            //service.GetRoles().ForEach(m => list.Add(new SelectListItem { Text = m.RoleName, Value = m.RoleId.ToString() }));
            ViewBag.Roles = list;
        }

        //[HttpPost]
        //public ActionResult List(string selectRole)
        //{
        //    SetDropDownValues();
        //    return View(service.ListByRole(int.Parse(selectRole)));
        //}

        //public ActionResult GetUsers()
        //{
        //    var list = service.List();
        //    return View(list);
        //}

        public PartialViewResult GetUsers(string selectRole = "")
        {
            if (string.IsNullOrEmpty(selectRole))
                return PartialView(service.List());
            else
            return PartialView(service.ListByRole(int.Parse(selectRole)));
        }

        public JsonResult GetJsonUser(string selectRole = "")
        {
            if (string.IsNullOrEmpty(selectRole))
                return Json(service.List());
            else
                return Json(service.ListByDynamic(int.Parse(selectRole)));
        }



    }
}