﻿using Lec5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lec5.Controllers
{
    public class UserApiController : ApiController
    {
        public List<User> Get()
        {
            var service = new UserService();
            return service.List();
        }

        // POST: api/test
        public void Post(User value)
        {
            var service = new UserService();
            service.AddUser(value);
        }

        // PUT: api/test/5
        public void Put(User user)
        {
            var service = new UserService();
            service.UpdateUser(user);
        }

        // DELETE: api/test/5
        public void Delete(int id)
        {
            var service = new UserService();
            service.DeleteUser(id);
        }
    }
}
