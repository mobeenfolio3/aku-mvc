﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec5.Models
{
    public class UserService
    {
        public List<User> List()
        {
            var ent = new Entities();
           return  ent.Users.ToList();
        }


        public List<User> ListByRole(int roleId)
        {
            var ent = new Entities();
            return ent.Users.Where(m=>m.RoleId==roleId).ToList();
        }


        public IQueryable<dynamic> ListByDynamic(int roleId)
        {
            var ent = new Entities();
            return ent.Users.Where(m => m.RoleId == roleId).Select(m => new
            {
                UserName = m.UserName,
                Email = m.UserEmail
            });
        }

        internal void AddUser(User value)
        {
            var ent = new Entities();
            ent.Users.Add(value);
            ent.SaveChanges();
        }

        internal void UpdateUser(User value)
        {
            var ent = new Entities();
            var user =  ent.Users.First(m => m.UserId == value.UserId);
            user.UserName = value.UserName;
            user.UserEmail = value.UserEmail;
            ent.SaveChanges();
        }


        internal void DeleteUser(int Id)
        {
            var ent = new Entities();
            var user = ent.Users.First(m => m.UserId == Id);
            ent.Users.Remove(user);
            ent.SaveChanges();
        }

        public List<Role> GetRoles()
        {
            
            var ent = new Entities();
            return ent.Roles.ToList();
        }
    }
}