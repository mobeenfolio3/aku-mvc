﻿using Lec_2.Models.EM;
using Lec_2.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lec_2.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            var service = new SudentService();
            var students = service.GetList();
            return View(students);
        }

        public ActionResult ViewDetails(int id = 0)
        {
            if(id == 0)
                return RedirectToAction("Index");
            var service = new SudentService();
            var students = service.GetById(id);
            return View("Details",students);
        }

        public ActionResult Linq()
        {
            var service = new SudentService();
            var students = service.FilterByName("Saad");
            return View(students);
        }
    }
}