﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec_2.Models.EM
{
    public partial class Student
    {
        public string Guide()
        {
            return "I am from extension";
        }

        public string RegistrationNo
        {
            get
            {
                return RegistrationYear.ToString() + BatchNo.ToString("0000") + Id.ToString("000");
            }
        }
    }
}