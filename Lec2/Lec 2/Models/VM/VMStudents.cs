﻿using Lec_2.Models.EM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec_2.Models.VM
{
    public partial class VMStudents
    {
        public VMStudents()
        {
           
        }
        private int batchNo;
        private int year;
        private int id;
        public int Id { get { return id; } }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string RegistrationNo
        {
            get
            {
                return year.ToString() + batchNo.ToString("0000") + Id.ToString("000");
            }
        }


        public void Map(Student std)
        {
            Name = std.Name;
            FatherName = std.FatherName;
        }
    }
}