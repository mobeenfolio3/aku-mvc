﻿using Lec_2.Models.EM;
using Lec_2.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec_2.Models.Service
{
    public class SudentService
    {
        public List<VMStudents> GetList()
        {
            using (var context = new StudentEntities())
            {
                var lst = context.Students.ToList();
                var vmList = new List<VMStudents>();
                foreach (var item in lst)
                {
                    var vmStd = new VMStudents();
                    var name = item.Guide();
                    vmStd.Map(item);
                    vmList.Add(vmStd);
                }

                return vmList;
            }
        }

        public Student GetById(int id)
        {
            using (var context = new StudentEntities())
            {
                return context.Students.First(i => i.Id == id);
            }
        }

        public List<Student> FilterByName(string name)
        {
            using (var context = new StudentEntities())
            {
                var query = context.Students.Where(g => g.Name == name);
                query = query.OrderBy(o => o.FatherName);
                
                var data = query.Sum(p=>p.Id);
                return query.ToList();
            }

        }

        public List<VMStudents> FilterByFatherName(string name)
        {
            using (var context = new StudentEntities())
            {
                var query = from data in context.Students
                            where data.FatherName == name
                            orderby data.BatchNo
                            select new VMStudents{ Name = data.Name, FatherName = data.FatherName};

                var a = query.Where(g => g.FromParial == "").First();

                return query.ToList();
            }

        }
    }
}