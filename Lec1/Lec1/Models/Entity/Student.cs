﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec1.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public int ClassNo { get; set; }
        public string RollNo { get; set; }
        public List<StudentAddress> Addresses { get; set; }
    }
}