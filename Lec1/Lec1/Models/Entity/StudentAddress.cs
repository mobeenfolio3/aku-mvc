﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec1.Models
{
    public class StudentAddress
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}