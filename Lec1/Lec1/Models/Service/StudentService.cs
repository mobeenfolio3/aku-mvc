﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lec1.Models.Service
{
    public class StudentService
    {
        public List<Student> GetList()
        {
            var StdList = new List<Student>();
            StdList.Add(new Student { Id = 1, Name = "Ali", ClassNo = 5, FatherName = "Ahmed", RollNo = "BB111",
                Addresses = new List<StudentAddress> { new StudentAddress { Id = 1, Name = "Shahrah e faisal" }, new StudentAddress { Id = 2, Name = "Naz", Value = "Set" } } });
            
            StdList.Add(new Student { Id = 2, Name = "Asim", ClassNo = 3, FatherName = "Saad", RollNo = "BB112" });
            StdList.Add(new Student { Id = 3, Name = "Hamza", ClassNo = 4, FatherName = "Irani", RollNo = "BB113" });
            StdList.Add(new Student { Id = 4, Name = "Saad", ClassNo = 5, FatherName = "Saud", RollNo = "BB114" });
            StdList.Add(new Student { Id = 5, Name = "Kashif", ClassNo = 3, FatherName = "Bajwa", RollNo = "BB115" });
            return StdList;
        }

    }
}