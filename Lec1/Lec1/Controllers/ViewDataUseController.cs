﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lec1.Controllers
{
    public class ViewDataUseController : Controller
    {
        // GET: ViewDataUse
        public ActionResult Index()
        {
            var mydata = new Dictionary<string, string>();
            mydata.Add("Name", "Mobeen");
            mydata.Add("Class", "AKU1");
            mydata.Add("Degree", "Masters");
            mydata.Add("Distination", "Lahore");

            ViewData["data"] = mydata;
            return View();
        }
    }
}