﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lec1.Controllers
{
    public class AKUController : Controller
    {
        // GET: AKU
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MyInfo()
        {
            ViewBag.Name = "Super";
            ViewBag.Height = "11.11";
            return View();
        }
    }
}