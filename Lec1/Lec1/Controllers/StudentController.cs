﻿using Lec1.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lec1.Controllers
{
    public class StudentController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/AKU/Index.cshtml");
        }
        // GET: Student
        public ActionResult List()
        {
            var stdService = new StudentService();
            var listofStd = stdService.GetList();
            //ViewBag.Stds = listofStd;
            return View(listofStd);
        }

        public ActionResult ViewStudent(int id)
        {
            var stdService = new StudentService();
            var listofStd = stdService.GetList();
            var std = listofStd.First(a => a.Id == id);
            //ViewBag.Std = std;
            return View(std);
        }

        public ActionResult MyModel()
        {
            string num = "Test str";
            var t = new object();
            t = num;
            return View(num);
        }


    }
}