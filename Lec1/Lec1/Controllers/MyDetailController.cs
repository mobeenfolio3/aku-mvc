﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lec1.Controllers
{
    public class MyDetailController : Controller
    {
        // GET: MyDetail
        public ActionResult Index()
        {
            var mydata = new Dictionary<string, string>();
            mydata.Add("Name", "Mobeen");
            mydata.Add("Class", "AKU1");
            mydata.Add("Degree", "Masters");

            foreach (var item in mydata)
            {
                ViewData[item.Key] = item.Value;
            }
            
            ViewBag.Info = "Hi I am a new controller action";
            return View();
        }

        public ActionResult Records(int id)
        {
            if (id == 0)
                ViewBag.Info = "No Records found";
            else
                ViewBag.Info = "Records found " + id.ToString();
            return View();
        }

        public ActionResult AAAction(string name, string myclass)
        {
            ViewBag.Name = name;
            ViewBag.ClassName = myclass;    

            return View();
        }
    }
}